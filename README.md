Guru Point 05
=============

Instructions
------------

To get **one** extra credit point, you must do the following:

1. **Fork** this repository on [Bitbucket].

2. **Clone** your fork to a local machine.

3. In your clone, **create** a feature [branch] using [git] called
   `$NETID-GuruPoint05`.

4. In this new branch, **modify** this `README.md` to include one cool or
   interesting thing about yourself.  Follow the convention below.

5. **Commit** and **push** the new branch to your [Bitbucket] fork.

6. Submit a [pull request] to the original [Guru Point 05] repository.

You will get credit when your [pull request] has been accepted.

**Note**: The instructor will only accept a [pull request] that has no
conflicts with the existing **master** branch.  This means you need to be sure
your [pull request] can apply cleanly.  If it does not, you will need to
respond to the comments in the [pull request] to get your [branch] into proper
shape.

[Bitbucket]:    https://bitbucket.org/
[git]:          https://git-scm.com/
[branch]:       https://www.atlassian.com/git/tutorials/using-branches/
[pull request]: https://www.atlassian.com/git/tutorials/making-a-pull-request/how-it-works
[Guru Point 05]:https://bitbucket.org/CSE-20189-SP16/gurupoint_05

Peoples
-------

### Peter Bui

Some people call me Dr. Bui, but not my **mom**.  She says I'm not a real
doctor because I work on **computers** and not **people**.
